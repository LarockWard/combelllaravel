<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'product'], function(){
    Route::post("/create", "ProductController@createProduct");

    Route::put("/update", "ProductController@updateProduct");

    Route::delete("/delete", "ProductController@deleteProduct");

    Route::get("/getAll", "ProductController@getProducts");
});
