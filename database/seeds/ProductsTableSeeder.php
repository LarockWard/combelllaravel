<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Sunglasses',
            'price' => 25,
            'Category' => 'Accessories',
            'stock' => 12,
            'imageURL' => 'sunglasses.png',
            'brand' => 'zusss'
            
        ]);
    }
}
