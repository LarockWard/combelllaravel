<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function createProduct(Request $request){
        $product = Product::create($request->all());

        return response()->json($product);
    }

    public function updateProduct(Request $request, $id){
        $product  = DB::table('products')->where('id', $request->input('id'))->get();
        
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->category = $request->input('category');
        $product->stock = $request->input('stock');
        $product->imageURL = $request->input('imageURL');
        $product->brand = $request->input('brand');
    }

    public function deleteProduct($id){
        $product = DB::table('products')->where('id', $request->input('id'))->get();
        $product->delete();
        return response()->json('Removed product succesfully');
    }

    public function getProducts(){
        $products = Product::all();
        return response()->json($products);
    }
}
